"use strict";
// debugger;

let tab = function () {
    let tabTitle = document.querySelectorAll('.tabs-title');
    let tabsContent = document.querySelectorAll('.tabs-content-item');
    let tabActiveName;

    tabTitle.forEach(element => {
        element.addEventListener('click', selectTabTitle)
    });

    function selectTabTitle() {
        tabTitle.forEach(elem => {
            elem.classList.remove('active');
        });
        this.classList.add('active');
        tabActiveName = this.getAttribute('data-tab-name')
        selectTabsContent(tabActiveName)
        console.log(tabActiveName)
    }

    function selectTabsContent() {
        tabsContent.forEach(item => {
            item.classList.contains(tabActiveName) ?
                item.classList.add('active') :
                item.classList.remove('active');
        })
    }
};

tab();